program Integrador;

uses
  Vcl.Forms,
  U_Principal in 'U_Principal.pas' {FrmPrincipal},
  dllcompanytec in 'dllcompanytec.pas',
  Vcl.Themes,
  Vcl.Styles,
  U_Cliente in 'U_Cliente.pas' {FrmCliente},
  U_Produto in 'U_Produto.pas' {FrmProduto},
  U_Categoria in 'U_Categoria.pas' {FrmCategoria},
  U_CategoriaProdutos in 'U_CategoriaProdutos.pas' {FrmCategoriaProdutos},
  U_InformaValor in 'U_InformaValor.pas' {FrmInformaValor},
  U_Bicos in 'U_Bicos.pas' {FrmBicos},
  U_Bomba in 'U_Bomba.pas' {FrmBomba},
  U_BicoDetalhe in 'U_BicoDetalhe.pas' {FrmBicoDetalhe};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.CreateForm(TFrmBicoDetalhe, FrmBicoDetalhe);
  Application.Run;
end.
