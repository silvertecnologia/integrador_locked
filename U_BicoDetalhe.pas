unit U_BicoDetalhe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.DBCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmBicoDetalhe = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdtHexa: TEdit;
    EdtCodigo: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    DBLkpProduto: TDBLookupComboBox;
    QryProdutos: TFDQuery;
    DtsProdutos: TDataSource;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmBicoDetalhe: TFrmBicoDetalhe;

implementation

{$R *.dfm}

procedure TFrmBicoDetalhe.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    if Trim(EdtCodigo.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o c�digo do bico.');
      EdtCodigo.SetFocus;
    end else if Trim(EdtHexa.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o c�digo hexa.');
      EdtHexa.SetFocus;
    end else if Trim(DBLkpProduto.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o produto.');
      DBLkpProduto.SetFocus;
    end;
  end;
end;

procedure TFrmBicoDetalhe.FormCreate(Sender: TObject);
begin
  QryProdutos.Open;
end;

end.
