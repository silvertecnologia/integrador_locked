unit U_Bicos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.DBCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.Provider, Datasnap.DBClient, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.UITypes;

type
  TFrmBicos = class(TForm)
    DbgBicos: TDBGrid;
    Panel7: TPanel;
    DBNavigator3: TDBNavigator;
    BtnAlterar: TBitBtn;
    BtnRemover: TBitBtn;
    BtnIncluir: TBitBtn;
    Panel1: TPanel;
    BtnFechar: TBitBtn;
    QryBicos: TFDQuery;
    CdsBicos: TClientDataSet;
    DspBicos: TDataSetProvider;
    DtsBicos: TDataSource;
    CdsBicosid_bomba_bico: TAutoIncField;
    CdsBicosid_bomba: TIntegerField;
    CdsBicosid_bico: TIntegerField;
    CdsBicosid_produto: TIntegerField;
    CdsBicoshexa: TStringField;
    DtsProduto: TDataSource;
    QryProduto: TFDQuery;
    CdsBicosdescr_produto: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure BtnRemoverClick(Sender: TObject);
    procedure BtnIncluirClick(Sender: TObject);
    procedure BtnAlterarClick(Sender: TObject);
  private
    _IdBomba: Integer;
  public
    procedure CarregarBicos(IdBomba: Integer);
  end;

var
  FrmBicos: TFrmBicos;

implementation

{$R *.dfm}

uses U_Principal, U_Bomba, U_BicoDetalhe;

procedure TFrmBicos.BtnAlterarClick(Sender: TObject);
begin
  if not CdsBicos.IsEmpty then begin
    FrmBicoDetalhe := TFrmBicoDetalhe.Create(nil);
    FrmBicoDetalhe.EdtCodigo.Text := CdsBicos.FieldByName('ID_BICO').AsString;
    FrmBicoDetalhe.EdtHexa.Text := CdsBicos.FieldByName('HEXA').AsString;
    FrmBicoDetalhe.QryProdutos.Locate('ID_PRODUTO', CdsBicos.FieldByName('ID_PRODUTO').AsInteger, []);
    FrmBicoDetalhe.DBLkpProduto.KeyValue := CdsBicos.FieldByName('ID_PRODUTO').AsInteger;
    try
      if FrmBicoDetalhe.ShowModal = mrOk then begin
        CdsBicos.Edit;
        CdsBicos.FieldByName('ID_BICO').AsString := FrmBicoDetalhe.EdtCodigo.Text;
        CdsBicos.FieldByName('HEXA').AsString := FrmBicoDetalhe.EdtHexa.Text;
        CdsBicos.FieldByName('ID_PRODUTO').AsInteger := FrmBicoDetalhe.QryProdutos.FieldByName('ID_PRODUTO').AsInteger;
        CdsBicos.Post;
        CdsBicos.ApplyUpdates(-1);
      end;
    finally
      FrmBicoDetalhe.Free;
    end;
  end;
end;

procedure TFrmBicos.BtnIncluirClick(Sender: TObject);
begin
  FrmBicoDetalhe := TFrmBicoDetalhe.Create(nil);
  try
    if FrmBicoDetalhe.ShowModal = mrOk then begin
      CdsBicos.Insert;
      CdsBicos.FieldByName('ID_BICO').AsString := FrmBicoDetalhe.EdtCodigo.Text;
      CdsBicos.FieldByName('HEXA').AsString := FrmBicoDetalhe.EdtHexa.Text;
      CdsBicos.FieldByName('ID_PRODUTO').AsInteger := FrmBicoDetalhe.QryProdutos.FieldByName('ID_PRODUTO').AsInteger;
      CdsBicos.FieldByName('ID_BOMBA').AsInteger := _IdBomba;
      CdsBicos.Post;
      CdsBicos.ApplyUpdates(-1);
      CdsBicos.Close;
      CdsBicos.Open;
      CdsBicos.Locate('HEXA', FrmBicoDetalhe.EdtHexa.Text, []);
    end;
  finally
    FrmBicoDetalhe.Free;
  end;
end;

procedure TFrmBicos.BtnRemoverClick(Sender: TObject);
begin
  if not CdsBicos.IsEmpty then begin
    if MessageDlg('Confirma remo��o do bico?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      CdsBicos.Delete;
      CdsBicos.ApplyUpdates(-1);
    end;
  end;
end;

procedure TFrmBicos.CarregarBicos(IdBomba: Integer);
begin
  _IdBomba := IdBomba;
  CdsBicos.Close;
  CdsBicos.ParamByName('ID_BOMBA').AsInteger := IdBomba;
  CdsBicos.Open;
end;

procedure TFrmBicos.FormCreate(Sender: TObject);
begin
  QryProduto.Open;
end;

end.
