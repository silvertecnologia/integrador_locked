unit U_Bomba;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmBomba = class(TForm)
    Label1: TLabel;
    EdtHexa: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmBomba: TFrmBomba;

implementation

{$R *.dfm}

procedure TFrmBomba.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    if Trim(EdtHexa.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o valor do hexadecimal.');
      EdtHexa.SetFocus;
    end;
  end;
end;

end.
