unit U_Categoria;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFrmCategoria = class(TForm)
    Label1: TLabel;
    EdtDescricao: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    procedure CarregarProdutos(IdCategoria: Integer);
  end;

var
  FrmCategoria: TFrmCategoria;

implementation

{$R *.dfm}

uses U_Produto, U_Principal;

procedure TFrmCategoria.CarregarProdutos(IdCategoria: Integer);

begin

end;

procedure TFrmCategoria.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    if Trim(EdtDescricao.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe a descri��o.');
      EdtDescricao.SetFocus;
    end;
  end;
end;

end.
