unit U_CategoriaProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmCategoriaProdutos = class(TForm)
    LstProdutos: TListView;
    Label2: TLabel;
    QryProdutos: TFDQuery;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnAtualizar: TBitBtn;
    QryValorProdutoCategoria: TFDQuery;
    QryAtualizarValorProdutoCategoria: TFDQuery;
    QryInserirValorProdutoCategoria: TFDQuery;
    procedure LstProdutosDblClick(Sender: TObject);
  private
    _IdCategoria: Integer;
  public
    procedure CarregarProdutos(IdCategoria: Integer);
  end;

var
  FrmCategoriaProdutos: TFrmCategoriaProdutos;

implementation

{$R *.dfm}

uses U_InformaValor;

{ TFrmCategoriaProdutos }

procedure TFrmCategoriaProdutos.CarregarProdutos(IdCategoria: Integer);
var Item: TListItem;
begin
  _IdCategoria := IdCategoria;
  QryProdutos.Close;
  QryProdutos.Open;
  while not (QryProdutos.Eof) do begin
    Item := LstProdutos.Items.Add;
    Item.Caption := QryProdutos.FieldByName('DESCRICAO').AsString;
    Item.Data := TObject(QryProdutos.FieldByName('ID_PRODUTO').AsInteger);
    QryValorProdutoCategoria.Close;
    QryValorProdutoCategoria.ParamByName('ID_CATEGORIA').AsInteger := IdCategoria;
    QryValorProdutoCategoria.ParamByName('ID_PRODUTO').AsInteger := QryProdutos.FieldByName('ID_PRODUTO').AsInteger;
    QryValorProdutoCategoria.Open;
    if QryValorProdutoCategoria.IsEmpty then
      Item.SubItems.Add('-')
    else
      Item.SubItems.Add(FormatFloat('#,##0.000', QryValorProdutoCategoria.FieldByName('VALOR_FIDELIDADE').AsCurrency));
    QryProdutos.Next;
  end;
end;

procedure TFrmCategoriaProdutos.LstProdutosDblClick(Sender: TObject);
begin
  if (LstProdutos.Selected <> nil) then begin
    FrmInformaValor := TFrmInformaValor.Create(nil);
    try
      if LstProdutos.Selected.SubItems[0] <> '-' then
        FrmInformaValor.EdtValor.Text := LstProdutos.Selected.SubItems[0];
      if FrmInformaValor.ShowModal = mrOk then begin
        if LstProdutos.Selected.SubItems[0] <> '-' then begin
          QryAtualizarValorProdutoCategoria.Close;
          QryAtualizarValorProdutoCategoria.ParamByName('ID_CATEGORIA').AsInteger := _IdCategoria;
          QryAtualizarValorProdutoCategoria.ParamByName('ID_PRODUTO').AsInteger := Integer(LstProdutos.Selected.Data);
          QryAtualizarValorProdutoCategoria.ParamByName('VALOR_FIDELIDADE').AsFloat := StrToFloat(FrmInformaValor.EdtValor.Text);
          QryAtualizarValorProdutoCategoria.ExecSQL;
        end else begin
          QryInserirValorProdutoCategoria.Close;
          QryInserirValorProdutoCategoria.ParamByName('ID_CATEGORIA').AsInteger := _IdCategoria;
          QryInserirValorProdutoCategoria.ParamByName('ID_PRODUTO').AsInteger := Integer(LstProdutos.Selected.Data);
          QryInserirValorProdutoCategoria.ParamByName('VALOR_FIDELIDADE').AsFloat := StrToFloat(FrmInformaValor.EdtValor.Text);
          QryInserirValorProdutoCategoria.ExecSQL;
        end;
        LstProdutos.Selected.SubItems[0] := FormatFloat('#,##0.000', StrToFloat(FrmInformaValor.EdtValor.Text));
      end;
    finally
      FrmInformaValor.Free;
    end;
  end;
end;

end.
