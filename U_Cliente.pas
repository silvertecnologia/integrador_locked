unit U_Cliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.DBCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFrmCliente = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtCPF: TEdit;
    EdtNome: TEdit;
    EdtTelefone: TEdit;
    EdtTag: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    Label5: TLabel;
    DBLkpCategoria: TDBLookupComboBox;
    QryCategorias: TFDQuery;
    DtsCategorias: TDataSource;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCliente: TFrmCliente;

implementation

{$R *.dfm}

uses U_Principal;

procedure TFrmCliente.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    if Trim(EdtNome.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o nome.');
      EdtNome.SetFocus;
    end else if Trim(EdtTag.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe a tag.');
      EdtTag.SetFocus;
    end;
  end;
end;

procedure TFrmCliente.FormCreate(Sender: TObject);
begin
  QryCategorias.Open;
end;

end.
