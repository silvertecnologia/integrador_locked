unit U_InformaValor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmInformaValor = class(TForm)
    Label1: TLabel;
    EdtValor: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmInformaValor: TFrmInformaValor;

implementation

{$R *.dfm}

procedure TFrmInformaValor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    try
      StrToFloat(EdtValor.Text);
    except
      CanClose := False;
      ShowMessage('Informe um valor v�lido.');
      EdtValor.SetFocus;
    end;
  end;
end;

end.
