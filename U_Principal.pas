unit U_Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, StrUtils,
  Vcl.ComCtrls, Vcl.Samples.Spin, dllcompanytec, Data.DB, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.DBCtrls, FireDAC.Phys.MSSQL,
  FireDAC.Phys.MSSQLDef, System.UITypes, Datasnap.Provider, Datasnap.DBClient,
  IniFiles;

type
  TStringArray = array of string;

  TFrmPrincipal = class(TForm)
    GrpConexao: TGroupBox;
    EdtIP: TEdit;
    Label1: TLabel;
    BtnConectar: TBitBtn;
    GrpStatus: TGroupBox;
    LblStatus: TLabel;
    TimerStatus: TTimer;
    TimerPrincipal: TTimer;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EdtBico: TEdit;
    BtnAlterarValor: TBitBtn;
    Valor: TLabel;
    Decimais: TLabel;
    SpnDecimais: TSpinEdit;
    EdtPreco: TEdit;
    TimerSituacao: TTimer;
    TimerFID: TTimer;
    FDConn: TFDConnection;
    QryClientes: TFDQuery;
    DtsCliente: TDataSource;
    PgcPrincipal: TPageControl;
    TbsBicos: TTabSheet;
    ScrollBox: TScrollBox;
    TbsAbastecimentos: TTabSheet;
    LstIntegracao: TListView;
    TbsClientes: TTabSheet;
    Panel1: TPanel;
    DBNavigator1: TDBNavigator;
    BtnAlterarCliente: TBitBtn;
    BtnRemoverCliente: TBitBtn;
    DbgClientes: TDBGrid;
    BtnIncluirCliente: TBitBtn;
    BtnAtualizarCliente: TBitBtn;
    CdsCliente: TClientDataSet;
    DspCliente: TDataSetProvider;
    CdsClienteCODIGO: TIntegerField;
    CdsClienteNOME: TStringField;
    CdsClienteCPF: TStringField;
    CdsClienteTELEFONE: TStringField;
    CdsClienteTAG: TStringField;
    TbsFidelidade: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    LstHistoricoFidelidade: TListView;
    Panel4: TPanel;
    Panel5: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdtTag: TEdit;
    EdtNome: TEdit;
    EdtCPF: TEdit;
    EdtTelefone: TEdit;
    QryProdutosClientePorTAG: TFDQuery;
    Label12: TLabel;
    EdtPorta: TEdit;
    BtnDesconectar: TBitBtn;
    TbsProdutos: TTabSheet;
    TbsBombas: TTabSheet;
    Panel6: TPanel;
    DBNavigator2: TDBNavigator;
    BtnAlterarProduto: TBitBtn;
    BtnRemoverProduto: TBitBtn;
    BtnIncluirProduto: TBitBtn;
    BtnAtualizarProduto: TBitBtn;
    DbgProdutos: TDBGrid;
    QryProduto: TFDQuery;
    CdsProduto: TClientDataSet;
    DspProduto: TDataSetProvider;
    DtsProduto: TDataSource;
    CdsProdutoid_produto: TAutoIncField;
    CdsProdutodescricao: TStringField;
    CdsProdutovalor_padrao: TCurrencyField;
    TbsCategorias: TTabSheet;
    Panel7: TPanel;
    DBNavigator3: TDBNavigator;
    BtnAlterarCategoria: TBitBtn;
    BtnRemoverCategoria: TBitBtn;
    BtnIncluirCategoria: TBitBtn;
    BtnAtualizarCategoria: TBitBtn;
    DbgCategorias: TDBGrid;
    QryCategoria: TFDQuery;
    CdsCategoria: TClientDataSet;
    DspCategoria: TDataSetProvider;
    DtsCategoria: TDataSource;
    CdsCategoriaid_cliente_categoria: TAutoIncField;
    CdsCategoriadescricao: TStringField;
    BtnValoresProdutos: TBitBtn;
    CdsClienteid_categoria: TIntegerField;
    CdsClienteDESCR_CATEGORIA: TStringField;
    Panel8: TPanel;
    DBNavigator4: TDBNavigator;
    BtnAlterarBomba: TBitBtn;
    BtnRemoverBomba: TBitBtn;
    BtnIncluirBomba: TBitBtn;
    BtnAtualizarBomba: TBitBtn;
    BtnBicos: TBitBtn;
    DbgBombas: TDBGrid;
    CdsBomba: TClientDataSet;
    QryBomba: TFDQuery;
    DspBomba: TDataSetProvider;
    DtsBomba: TDataSource;
    CdsBombaid_bomba: TAutoIncField;
    CdsBombahexa: TStringField;
    QryDadosBombaFidelidade: TFDQuery;
    QryPrecoPadraoBico: TFDQuery;
    procedure BtnConectarClick(Sender: TObject);
    procedure TimerStatusTimer(Sender: TObject);
    procedure TimerPrincipalTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnAlterarValorClick(Sender: TObject);
    procedure TimerSituacaoTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerFIDTimer(Sender: TObject);
    procedure PgcPrincipalChange(Sender: TObject);
    procedure BtnAtualizarClienteClick(Sender: TObject);
    procedure BtnRemoverClienteClick(Sender: TObject);
    procedure BtnIncluirClienteClick(Sender: TObject);
    procedure BtnAlterarClienteClick(Sender: TObject);
    procedure FDConnBeforeConnect(Sender: TObject);
    procedure BtnDesconectarClick(Sender: TObject);
    procedure BtnAtualizarProdutoClick(Sender: TObject);
    procedure BtnRemoverProdutoClick(Sender: TObject);
    procedure BtnIncluirProdutoClick(Sender: TObject);
    procedure BtnAlterarProdutoClick(Sender: TObject);
    procedure BtnIncluirCategoriaClick(Sender: TObject);
    procedure BtnAlterarCategoriaClick(Sender: TObject);
    procedure BtnRemoverCategoriaClick(Sender: TObject);
    procedure BtnAtualizarCategoriaClick(Sender: TObject);
    procedure BtnValoresProdutosClick(Sender: TObject);
    procedure DbgCategoriasDblClick(Sender: TObject);
    procedure DbgClientesDblClick(Sender: TObject);
    procedure DbgProdutosDblClick(Sender: TObject);
    procedure BtnBicosClick(Sender: TObject);
    procedure BtnAtualizarBombaClick(Sender: TObject);
    procedure BtnIncluirBombaClick(Sender: TObject);
    procedure BtnAlterarBombaClick(Sender: TObject);
    procedure BtnRemoverBombaClick(Sender: TObject);
    procedure DbgBombasDblClick(Sender: TObject);
  private
    BicosAbastecendoComDesconto: TStringList;
    function ErrorToString(Erro: Error): String;
    procedure CriaBicos;
    procedure GerenciaTimers(Ativado: Boolean);
    function HexaToCodBico(Hexa: String): Integer;
    function PosicaoToGrupoBicos(Posicao: Integer): TStringArray;
    procedure ConfiguraSituacaoBico(Hexa: String; CodBico: Integer; Status: MultiStatus; Posicao: Integer);
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

uses U_Cliente, U_Produto, U_Categoria, U_CategoriaProdutos, U_Bicos, U_Bomba;

procedure TFrmPrincipal.BtnDesconectarClick(Sender: TObject);
begin
  if FechaSocket then begin
    GerenciaTimers(False);
    BtnConectar.Enabled := True;
    BtnDesconectar.Enabled := False;
    LblStatus.Caption := 'Desconectado';
    LblStatus.Font.Color := clRed;
  end else
    ShowMessage('Erro a fechar comunica��o.');
end;

procedure TFrmPrincipal.BtnAlterarBombaClick(Sender: TObject);
begin
  if not CdsBomba.IsEmpty then begin
    FrmBomba := TFrmBomba.Create(nil);
    FrmBomba.EdtHexa.Text := CdsBomba.FieldByName('HEXA').AsString;
    try
      if FrmBomba.ShowModal = mrOk then begin
        CdsBomba.Edit;
        CdsBomba.FieldByName('HEXA').AsString := FrmBomba.EdtHexa.Text;
        CdsBomba.Post;
        CdsBomba.ApplyUpdates(-1);
      end;
    finally
      FrmBomba.Free;
    end;
  end;
end;

procedure TFrmPrincipal.BtnAlterarCategoriaClick(Sender: TObject);
begin
  if not CdsCategoria.IsEmpty then begin
    FrmCategoria := TFrmCategoria.Create(nil);
    FrmCategoria.EdtDescricao.Text := CdsCategoria.FieldByName('DESCRICAO').AsString;
    try
      if FrmCategoria.ShowModal = mrOk then begin
        CdsCategoria.Edit;
        CdsCategoria.FieldByName('DESCRICAO').AsString := FrmCategoria.EdtDescricao.Text;
        CdsCategoria.Post;
        CdsCategoria.ApplyUpdates(-1);
      end;
    finally
      FrmCategoria.Free;
    end;
  end;
end;

procedure TFrmPrincipal.BtnAlterarClienteClick(Sender: TObject);
begin
  if not CdsCliente.IsEmpty then begin
    FrmCliente := TFrmCliente.Create(nil);
    FrmCliente.EdtNome.Text := CdsCliente.FieldByName('NOME').AsString;
    FrmCliente.EdtTag.Text := CdsCliente.FieldByName('TAG').AsString;
    FrmCliente.EdtTelefone.Text := CdsCliente.FieldByName('TELEFONE').AsString;
    FrmCliente.EdtCPF.Text := CdsCliente.FieldByName('CPF').AsString;
    FrmCliente.QryCategorias.Locate('ID_CLIENTE_CATEGORIA', CdsCliente.FieldByName('ID_CATEGORIA').AsInteger, []);
    FrmCliente.DBLkpCategoria.KeyValue := CdsCliente.FieldByName('ID_CATEGORIA').AsInteger;
    try
      if FrmCliente.ShowModal = mrOk then begin
        CdsCliente.Edit;
        CdsCliente.FieldByName('NOME').AsString := FrmCliente.EdtNome.Text;
        CdsCliente.FieldByName('TAG').AsString := FrmCliente.EdtTag.Text;
        if (Trim(FrmCliente.EdtCPF.Text) <> EmptyStr) then
          CdsCliente.FieldByName('CPF').AsString := FrmCliente.EdtCPF.Text
        else
          CdsCliente.FieldByName('CPF').Clear;
        if (Trim(FrmCliente.EdtTelefone.Text) <> EmptyStr) then
          CdsCliente.FieldByName('TELEFONE').AsString := FrmCliente.EdtTelefone.Text
        else
          CdsCliente.FieldByName('TELEFONE').Clear;
        CdsCliente.FieldByName('ID_CATEGORIA').Clear;
        if (Trim(FrmCliente.DBLkpCategoria.Text) <> EmptyStr) then
          CdsCliente.FieldByName('ID_CATEGORIA').AsInteger := FrmCliente.QryCategorias.FieldByName('ID_CLIENTE_CATEGORIA').AsInteger;
        CdsCliente.Post;
        CdsCliente.ApplyUpdates(-1);
      end;
    finally
      FrmCliente.Free;
    end;
  end;
end;

procedure TFrmPrincipal.BtnAlterarProdutoClick(Sender: TObject);
begin
  if not CdsProduto.IsEmpty then begin
    FrmProduto := TFrmProduto.Create(nil);
    FrmProduto.EdtDescricao.Text := CdsProduto.FieldByName('DESCRICAO').AsString;
    FrmProduto.EdtValorPadrao.Text := CdsProduto.FieldByName('VALOR_PADRAO').AsString;
    try
      if FrmProduto.ShowModal = mrOk then begin
        CdsProduto.Edit;
        CdsProduto.FieldByName('DESCRICAO').AsString := FrmProduto.EdtDescricao.Text;
        CdsProduto.FieldByName('VALOR_PADRAO').Value := StrToFloat(FrmProduto.EdtValorPadrao.Text);
        CdsProduto.Post;
        CdsProduto.ApplyUpdates(-1);
      end;
    finally
      FrmProduto.Free;
    end;
  end;
end;

procedure TFrmPrincipal.BtnAlterarValorClick(Sender: TObject);
var Erro: Error;
Bico: byte;
begin
  if LblStatus.Caption = 'Desconectado' then
    ShowMessage('Conecte-se ao concentrador antes de realizar a altera��o de pre�o.')
  else begin
    if (EdtBico.Text) = EmptyStr then
      ShowMessage('Informe o bico.')
    else if (EdtPreco.Text) = EmptyStr then
      ShowMessage('Informe o preco.')
    else begin
      try
        StrToFloat(EdtPreco.Text);
        Bico := StrToInt('$' + EdtBico.Text);
        Erro := SetPrice(Bico, StrToFloat(EdtPreco.Text), Byte.Parse(SpnDecimais.Text));
        ShowMessage(ErrorToString(Erro));
      except
        ShowMessage('Pre�o inv�lido.');
      end;
    end;
  end;
end;

procedure TFrmPrincipal.BtnAtualizarBombaClick(Sender: TObject);
begin
  CdsBomba.Close;
  CdsBomba.Open;
end;

procedure TFrmPrincipal.BtnAtualizarCategoriaClick(Sender: TObject);
begin
  CdsCategoria.Close;
  CdsCategoria.Open;
end;

procedure TFrmPrincipal.BtnAtualizarClienteClick(Sender: TObject);
begin
  CdsCliente.Close;
  CdsCliente.Open;
end;

procedure TFrmPrincipal.BtnAtualizarProdutoClick(Sender: TObject);
begin
  CdsProduto.Close;
  CdsProduto.Open;
end;

procedure TFrmPrincipal.BtnBicosClick(Sender: TObject);
begin
  if not CdsBomba.IsEmpty then begin
    FrmBicos := TFrmBicos.Create(nil);
    try
      FrmBicos.CarregarBicos(CdsBomba.FieldByName('ID_BOMBA').AsInteger);
      FrmBicos.ShowModal;
    finally
      FrmBicos.Free;
    end;
  end;
end;

procedure TFrmPrincipal.BtnConectarClick(Sender: TObject);
var IP: AnsiString;
IniFile: TIniFile;
begin
  IP := AnsiString(EdtIP.Text);
  LblStatus.Font.Color := clBlue;
  LblStatus.Caption := 'Conectando...';
  Update;
  Application.ProcessMessages;
  try
    IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Configuracoes.ini');
    try
      IniFile.WriteString('CONFIGURACOES', 'IP', EdtIP.Text);
      IniFile.WriteString('CONFIGURACOES', 'PORTA', EdtPorta.Text);
    finally
      IniFile.Free;
    end;
  except end;
  if InicializaSocket2(IP, StrToInt(EdtPorta.Text)) then begin
    GerenciaTimers(True);
    BtnConectar.Enabled := False;
    BtnDesconectar.Enabled := True;
    LblStatus.Caption := 'Conectado';
    LblStatus.Font.Color := clGreen;
  end else begin
    GerenciaTimers(False);
    BtnConectar.Enabled := True;
    BtnDesconectar.Enabled := False;
    LblStatus.Caption := 'Desconectado';
    LblStatus.Font.Color := clRed;
    ShowMessage('Erro na abertura de comunica��o.');
  end;
end;

procedure TFrmPrincipal.BtnIncluirBombaClick(Sender: TObject);
begin
  FrmBomba := TFrmBomba.Create(nil);
  try
    if FrmBomba.ShowModal = mrOk then begin
      CdsBomba.Insert;
      CdsBomba.FieldByName('HEXA').AsString := FrmBomba.EdtHexa.Text;
      CdsBomba.Post;
      CdsBomba.ApplyUpdates(-1);
      CdsBomba.Close;
      CdsBomba.Open;
      CdsBomba.Locate('HEXA', FrmBomba.EdtHexa.Text, []);
    end;
  finally
    FrmBomba.Free;
  end;
end;

procedure TFrmPrincipal.BtnIncluirCategoriaClick(Sender: TObject);
begin
  FrmCategoria := TFrmCategoria.Create(nil);
  try
    if FrmCategoria.ShowModal = mrOk then begin
      CdsCategoria.Insert;
      CdsCategoria.FieldByName('DESCRICAO').AsString := FrmCategoria.EdtDescricao.Text;
      CdsCategoria.Post;
      CdsCategoria.ApplyUpdates(-1);
      CdsCategoria.Close;
      CdsCategoria.Open;
      CdsCategoria.Locate('DESCRICAO', FrmCategoria.EdtDescricao.Text, []);
    end;
  finally
    FrmCategoria.Free;
  end;
end;

procedure TFrmPrincipal.BtnIncluirClienteClick(Sender: TObject);
begin
  FrmCliente := TFrmCliente.Create(nil);
  try
    if FrmCliente.ShowModal = mrOk then begin
      CdsCliente.Insert;
      CdsCliente.FieldByName('NOME').AsString := FrmCliente.EdtNome.Text;
      CdsCliente.FieldByName('TAG').AsString := FrmCliente.EdtTag.Text;
      CdsCliente.FieldByName('CPF').Clear;
      if (Trim(FrmCliente.EdtCPF.Text) <> EmptyStr) then
        CdsCliente.FieldByName('CPF').AsString := FrmCliente.EdtCPF.Text;
      CdsCliente.FieldByName('TELEFONE').Clear;
      if (Trim(FrmCliente.EdtTelefone.Text) <> EmptyStr) then
        CdsCliente.FieldByName('TELEFONE').AsString := FrmCliente.EdtTelefone.Text;
      CdsCliente.FieldByName('ID_CATEGORIA').Clear;
      if (Trim(FrmCliente.DBLkpCategoria.Text) <> EmptyStr) then
        CdsCliente.FieldByName('ID_CATEGORIA').AsInteger := FrmCliente.QryCategorias.FieldByName('ID_CLIENTE_CATEGORIA').AsInteger;
      CdsCliente.Post;
      CdsCliente.ApplyUpdates(-1);
      CdsCliente.Close;
      CdsCliente.Open;
      CdsCliente.Locate('NOME', FrmCliente.EdtNome.Text, []);
    end;
  finally
    FrmCliente.Free;
  end;
end;

procedure TFrmPrincipal.BtnIncluirProdutoClick(Sender: TObject);
begin
  FrmProduto := TFrmProduto.Create(nil);
  try
    if FrmProduto.ShowModal = mrOk then begin
      CdsProduto.Insert;
      CdsProduto.FieldByName('DESCRICAO').AsString := FrmProduto.EdtDescricao.Text;
      CdsProduto.FieldByName('VALOR_PADRAO').Value := StrToFloat(FrmProduto.EdtValorPadrao.Text);
      CdsProduto.Post;
      CdsProduto.ApplyUpdates(-1);
      CdsProduto.Close;
      CdsProduto.Open;
      CdsProduto.Locate('DESCRICAO', FrmProduto.EdtDescricao.Text, []);
    end;
  finally
    FrmProduto.Free;
  end;
end;

procedure TFrmPrincipal.BtnRemoverBombaClick(Sender: TObject);
begin
  if not CdsBomba.IsEmpty then begin
    if MessageDlg('Confirma remo��o da bomba?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      CdsBomba.Delete;
      CdsBomba.ApplyUpdates(-1);
    end;
  end;
end;

procedure TFrmPrincipal.BtnRemoverCategoriaClick(Sender: TObject);
begin
  if not CdsCategoria.IsEmpty then begin
    if MessageDlg('Confirma remo��o da categoria?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      CdsCategoria.Delete;
      CdsCategoria.ApplyUpdates(-1);
    end;
  end;
end;

procedure TFrmPrincipal.BtnRemoverClienteClick(Sender: TObject);
begin
  if not CdsCliente.IsEmpty then begin
    if MessageDlg('Confirma remo��o do cliente?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      CdsCliente.Delete;
      CdsCliente.ApplyUpdates(-1);
    end;
  end;
end;

procedure TFrmPrincipal.BtnRemoverProdutoClick(Sender: TObject);
begin
  if not CdsProduto.IsEmpty then begin
    if MessageDlg('Confirma remo��o do produto?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      CdsProduto.Delete;
      CdsProduto.ApplyUpdates(-1);
    end;
  end;
end;

procedure TFrmPrincipal.BtnValoresProdutosClick(Sender: TObject);
begin
  if not CdsCategoria.IsEmpty then begin
    FrmCategoriaProdutos := TFrmCategoriaProdutos.Create(nil);
    try
      FrmCategoriaProdutos.CarregarProdutos(CdsCategoria.FieldByName('ID_CLIENTE_CATEGORIA').AsInteger);
      FrmCategoriaProdutos.ShowModal;
    finally
      FrmCategoriaProdutos.Free;
    end;
  end;
end;

procedure TFrmPrincipal.FDConnBeforeConnect(Sender: TObject);
var IniFile: TIniFile;
IniFileName: String;
begin
  IniFileName := ExtractFilePath(Application.ExeName) + 'Configuracoes.Ini';
  if FileExists(IniFileName) then begin
    IniFile := TIniFile.Create(IniFileName);
    try
      (FDConn.Params as TFDPhysMSSQLConnectionDefParams).Server := IniFile.ReadString('DATABASE', 'SERVER', 'LOCALHOST\SQLSERVER');
    finally
      IniFile.Free;
    end;
  end;
end;

procedure TFrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  BicosAbastecendoComDesconto.Free;
  FechaSocket;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
var IniFileName: String;
IniFile: TIniFile;
begin
  if Date > StrToDate('31/03/2022') then
    Halt;
  PgcPrincipal.ActivePageIndex := 0;
  CriaBicos;
  IniFileName := ExtractFilePath(Application.ExeName) + 'Configuracoes.Ini';
  if FileExists(IniFileName) then begin
    IniFile := TIniFile.Create(IniFileName);
    try
      EdtIP.Text := IniFile.ReadString('CONFIGURACOES', 'IP', '127.0.0.1');
      EdtPorta.Text := IniFile.ReadString('CONFIGURACOES', 'PORTA', '1771');
    finally
      IniFile.Free;
    end;
  end;
  BicosAbastecendoComDesconto := TStringList.Create;
end;

procedure TFrmPrincipal.TimerFIDTimer(Sender: TObject);
var Fid: IFid;
Item: TListItem;
Bico: Byte;
begin
  Fid := FidIdent;
  if Fid.Codigo <> '0' then begin
    Item := LstHistoricoFidelidade.Items.Add;
    Item.Caption := String(Fid.Codigo);
    QryProdutosClientePorTAG.Close;
    QryProdutosClientePorTAG.ParamByName('TAG').AsString := String(Fid.Codigo);
    QryProdutosClientePorTAG.Open;
    EdtTag.Text := String(Fid.Codigo);
    if QryProdutosClientePorTAG.IsEmpty then begin
      EdtNome.Text := 'Cliente n�o encontrado';
      EdtCPF.Clear;
      EdtTelefone.Clear;
    end else begin
      EdtNome.Text := QryProdutosClientePorTAG.FieldByName('NOME').AsString;
      EdtCPF.Text := QryProdutosClientePorTAG.FieldByName('CPF').AsString;
      EdtTelefone.Text := QryProdutosClientePorTAG.FieldByName('TELEFONE').AsString;
      while (not QryProdutosClientePorTAG.Eof) do begin
        Item := LstHistoricoFidelidade.Items.Add;
        Item.Caption := EdtTag.Text;
        Item.SubItems.Add(EdtNome.Text);
        Item.SubItems.Add(EdtCPF.Text);
        QryDadosBombaFidelidade.Close;
        QryDadosBombaFidelidade.ParamByName('HEXA_BICO').AsString := String(Fid.Endereco);
        QryDadosBombaFidelidade.ParamByName('ID_PRODUTO').AsInteger := QryProdutosClientePorTAG.FieldByName('ID_PRODUTO').AsInteger;
        QryDadosBombaFidelidade.Open;
        if not QryDadosBombaFidelidade.IsEmpty then begin
          Item.SubItems.Add(QryProdutosClientePorTAG.FieldByName('DESCRICAO').AsString);
          Item.SubItems.Add(IntToStr(HexaToCodBico(QryDadosBombaFidelidade.FieldByName('HEXA').AsString)));
          Item.SubItems.Add(QryDadosBombaFidelidade.FieldByName('HEXA').AsString);
          Item.SubItems.Add(FormatFloat('R$ #,##0.000', QryProdutosClientePorTAG.FieldByName('VALOR_FIDELIDADE').AsFloat));
          Bico := StrToInt('$' + QryDadosBombaFidelidade.FieldByName('HEXA').AsString);
          SetPrice(Bico, QryProdutosClientePorTAG.FieldByName('VALOR_FIDELIDADE').AsFloat, 3);
          BicosAbastecendoComDesconto.Add(String(QryDadosBombaFidelidade.FieldByName('HEXA').AsString));
        end else begin
          Item.SubItems.Add('-');
          Item.SubItems.Add('-');
          Item.SubItems.Add('-');
        end;
        QryProdutosClientePorTAG.Next;
      end;
    end;
  end;
  FidIncrementa;
end;

procedure TFrmPrincipal.TimerPrincipalTimer(Sender: TObject);
//var ab: AbastPAF2;
//Item: TListItem;
begin
(*  ab := LeAbastecimentoPAF2;
  if (ab.value) then begin
    Item := LstIntegracao.Items.Add;
    Item.Caption := FormatFloat('00000', ab.registro);
    Item.SubItems.Add(String(ab.codbico));
    Item.SubItems.Add(FloatToStr(ab.total_dinheiro));
    Item.SubItems.Add(FloatToStr(ab.total_litros));
    Item.SubItems.Add(FloatToStr(ab.PU));
    Item.SubItems.Add(IntToStr(ab.tempo));
    Item.SubItems.Add(FormatDateTime('dd/mm/yyyy', ab.datetime));
    Item.SubItems.Add(FormatDateTime('hh:nn:ss', ab.datetime));
    Item.SubItems.Add(ab.tag1);
    Item.SubItems.Add(ab.tag2);
    Incrementa;
  end; *)
end;

procedure TFrmPrincipal.TimerSituacaoTimer(Sender: TObject);
var Status: MultiStatus;
Situacoes: Online;
i, j, CodBico: Integer;
Bicos: TStringArray;
Hexa: String;
Bico: Byte;
PrecoPadrao: Double;
begin
  Status := LeStatus;
  for i := 1 to 48 do begin
    Bicos := PosicaoToGrupoBicos(i);
    for j := 0 to 3 do begin
      Hexa := Bicos[j];
      CodBico := HexaToCodBico(Hexa);
      if CodBico <> -1 then begin
        ConfiguraSituacaoBico(Hexa, CodBico, Status, i);
        if (BicosAbastecendoComDesconto.IndexOf(Hexa) <> -1) and
           (Status.Status[i] = StOptions.Abastecendo) then begin
          // Atualiza para o pre�o padr�o no bico
          QryPrecoPadraoBico.Close;
          QryPrecoPadraoBico.ParamByName('HEXA_BICO').AsString := Hexa;
          QryPrecoPadraoBico.Open;
          if (not QryPrecoPadraoBico.IsEmpty) and
             (not QryPrecoPadraoBico.FieldByName('VALOR_PADRAO').IsNull) then begin
              Bico := StrToInt('$' + Hexa);
              PrecoPadrao := QryPrecoPadraoBico.FieldByName('VALOR_PADRAO').AsFloat;
              SetPrice(Bico, PrecoPadrao, 3);
              BicosAbastecendoComDesconto.Delete(BicosAbastecendoComDesconto.IndexOf(Hexa));
             end;
        end;
      end;
    end;
  end;
  Situacoes := LeVisualizacao;
  for i := 1 to 48 do begin
    CodBico := HexaToCodBico(String(Situacoes.bico[i]));
    if CodBico <> -1 then
      TLabel(Self.FindComponent('LblLitragem' + FormatFloat('00', CodBico))).Caption := 'Litragem: ' + FloatToStr(Situacoes.litragem[i]);
  end;
end;

procedure TFrmPrincipal.ConfiguraSituacaoBico(Hexa: String; CodBico: Integer; Status: MultiStatus; Posicao: Integer);
begin
  case Status.Status[Posicao] of
      Livre: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Livre';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clGreen;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      Pronta: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Pronto';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clBlue;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      Falha: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Desconectado';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clPurple;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      Concluiu: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Concluiu';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clNavy;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      Abastecendo: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Abastecendo';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clRed;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      Bloqueada: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Bloqueada';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Color := clBlack;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
      SolicitaLib: begin
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Caption := FormatFloat('00', CodBico) + ' (Hexa ' + Hexa + ')';
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Caption := 'Solicita Lib.';
                TLabel(Self.FindComponent('LblBico' + FormatFloat('00', CodBico))).Font.Color := clOlive;
                TLabel(Self.FindComponent('LblStatus' + FormatFloat('00', CodBico))).Font.Color := clWhite;
             end;
    end;
end;

procedure TFrmPrincipal.TimerStatusTimer(Sender: TObject);
begin
  if not SocketOpen then begin
    BtnConectar.Enabled := True;
    BtnDesconectar.Enabled := False;
    LblStatus.Caption := 'Desconectado';
    LblStatus.Font.Color := clRed;
    GerenciaTimers(False);
    ShowMessage('Conex�o perdida.');
  end;
end;

function TFrmPrincipal.ErrorToString(Erro: Error): String;
begin
  if Erro = ErroString then
    result := 'Erro geral'
  else if Erro = None then
    result := 'Pre�o atualizado'
  else if Erro = ErroCodBico then
    result := 'Erro no c�digo do bico'
  else if Erro = ErroCaracterModo then
    result := 'Erro no modo caractere'
  else if Erro = ErroTimeout then
    result := 'Erro de time out'
  else if Erro = ErroResposta then
    result := 'Erro de resposta'
  else
    result := 'Erro de resposta';
end;

procedure TFrmPrincipal.CriaBicos;
var i, Topo, Esquerda: Integer;
Img: TImage; Panel: TPanel;
LblBico, LblStatus, LblLitragem: TLabel;
begin
  if not FileExists('bico.bmp') then
    ShowMessage('N�o foi poss�vel carregar os bicos.')
  else begin
    Topo     := 3;
    Esquerda := 3;
    for i := 1 to 48 do begin
      Panel := TPanel.Create(Self);
      Panel.Caption := EmptyStr;
      Panel.Parent := ScrollBox;
      Panel.BevelInner := bvLowered;
      Panel.Top := Topo;
      Panel.Left := Esquerda;
      Panel.Height := 85;
      Panel.Width  := 90;

      Img := TImage.Create(Self);
      Img.Name := 'ImgBico' + FormatFloat('00', i);
      Img.Parent := Panel;
      Img.Picture.LoadFromFile('bico.bmp');
      Img.AutoSize := True;
      Img.Top := 2;
      Img.Left := 24;

      LblBico := TLabel.Create(Self);
      LblBico.Name := 'LblBico' + FormatFloat('00', i);
      LblBico.Parent := Panel;
      LblBico.Caption := FormatFloat('00', i);
      LblBico.AutoSize := False;
      LblBico.Left := 2;
      LblBico.Width := 87;
      LblBico.Top := 46;
      LblBico.Font.Size := 7;
      LblBico.Alignment := taCenter;

      LblLitragem := TLabel.Create(Self);
      LblLitragem.Name := 'LblLitragem' + FormatFloat('00', i);
      LblLitragem.Parent := Panel;
      LblLitragem.Caption := 'Litragem: 0';
      LblLitragem.AutoSize := False;
      LblLitragem.Left := 2;
      LblLitragem.Width := 87;
      LblLitragem.Top := 58;
      LblLitragem.Font.Size := 7;
      LblLitragem.Alignment := taCenter;

      LblStatus := TLabel.Create(Self);
      LblStatus.Name := 'LblStatus' + FormatFloat('00', i);
      LblStatus.Parent := Panel;
      LblStatus.Caption := EmptyStr;
      LblStatus.AutoSize := False;
      LblStatus.Transparent := False;
      LblStatus.Left := 2;
      LblStatus.Width := 87;
      LblStatus.Top := 71;
      LblStatus.Height := 12;
      LblStatus.Font.Size := 7;
      LblStatus.Alignment := taCenter;

      Esquerda := Esquerda + 100;
      if (Esquerda + 100) > ScrollBox.Width then begin
        Esquerda := 2;
        Topo := Topo + 90;
      end;
    end;
  end;
end;

procedure TFrmPrincipal.DbgBombasDblClick(Sender: TObject);
begin
  BtnAlterarBombaClick(nil);
end;

procedure TFrmPrincipal.DbgCategoriasDblClick(Sender: TObject);
begin
  BtnAlterarCategoriaClick(nil);
end;

procedure TFrmPrincipal.DbgClientesDblClick(Sender: TObject);
begin
  BtnAlterarClienteClick(nil);
end;

procedure TFrmPrincipal.DbgProdutosDblClick(Sender: TObject);
begin
  BtnAlterarProdutoClick(nil);
end;

procedure TFrmPrincipal.GerenciaTimers(Ativado: Boolean);
begin
  TimerStatus.Enabled := Ativado;
  TimerPrincipal.Enabled := Ativado;
  TimerSituacao.Enabled := Ativado;
  TimerFID.Enabled := Ativado;
end;

procedure TFrmPrincipal.PgcPrincipalChange(Sender: TObject);
begin
  if (PgcPrincipal.ActivePage = TbsClientes) and (not CdsCliente.Active) then
    CdsCliente.Open
  else if (PgcPrincipal.ActivePage = TbsProdutos) and (not CdsProduto.Active) then
    CdsProduto.Open
  else if (PgcPrincipal.ActivePage = TbsCategorias) and (not CdsCategoria.Active) then
    CdsCategoria.Open
  else if (PgcPrincipal.ActivePage = TbsBombas) and (not CdsBomba.Active) then
    CdsBomba.Open;
end;

function TFrmPrincipal.HexaToCodBico(Hexa: String): Integer;
begin
  if Hexa = '50' then Result := 1
  else if Hexa = '10' then Result := 2
  else if Hexa = '51' then Result := 3
  else if Hexa = '11' then Result := 4
  else if Hexa = '0B' then Result := 5
  else if Hexa = '49' then Result := 6
  else if Hexa = '48' then Result := 7
  else if Hexa = '0A' then Result := 8
  else if Hexa = '09' then Result := 9
  else if Hexa = '08' then Result := 10
  else if Hexa = '0C' then Result := 11
  else if Hexa = '4C' then Result := 12
  else if Hexa = '0E' then Result := 13
  else if Hexa = '0F' then Result := 14
  else if Hexa = '4D' then Result := 15
  else if Hexa = '0D' then Result := 16
  else if Hexa = '04' then Result := 17
  else if Hexa = '44' then Result := 18
  else if Hexa = '06' then Result := 19
  else if Hexa = '07' then Result := 20
  else if Hexa = '45' then Result := 21
  else if Hexa = '05' then Result := 22
  else if Hexa = '17' then Result := 23
  else if Hexa = '55' then Result := 24
  else if Hexa = '15' then Result := 25
  else if Hexa = '14' then Result := 26
  else if Hexa = '54' then Result := 27
  else if Hexa = '16' then Result := 28
  else Result := -1;
end;

function TFrmPrincipal.PosicaoToGrupoBicos(Posicao: Integer): TStringArray;
begin
  SetLength(result, 4);
  Result[0] := EmptyStr; Result[1] := EmptyStr; Result[2] := EmptyStr; Result[3] := EmptyStr;
  if Posicao = 1 then begin
    Result[0] := '04'; Result[1] := '44'; Result[2] := '84'; Result[3] := 'C4';
  end else if Posicao = 2 then begin
    Result[0] := '05'; Result[1] := '45'; Result[2] := '85'; Result[3] := 'C5';
  end else if Posicao = 3 then begin
    Result[0] := '06'; Result[1] := '46'; Result[2] := '86'; Result[3] := 'C6';
  end else if Posicao = 4 then begin
    Result[0] := '07'; Result[1] := '47'; Result[2] := '87'; Result[3] := 'C7';
  end else if Posicao = 5 then begin
    Result[0] := '08'; Result[1] := '48'; Result[2] := '88'; Result[3] := 'C8';
  end else if Posicao = 6 then begin
    Result[0] := '09'; Result[1] := '49'; Result[2] := '89'; Result[3] := 'C9';
  end else if Posicao = 7 then begin
    Result[0] := '0A'; Result[1] := '4A'; Result[2] := '8A'; Result[3] := 'CA';
  end else if Posicao = 8 then begin
    Result[0] := '0B'; Result[1] := '4B'; Result[2] := '8B'; Result[3] := 'CB';
  end else if Posicao = 9 then begin
    Result[0] := '0C'; Result[1] := '4C'; Result[2] := '8C'; Result[3] := 'CC';
  end else if Posicao = 10 then begin
    Result[0] := '0D'; Result[1] := '4D'; Result[2] := '8D'; Result[3] := 'CD';
  end else if Posicao = 11 then begin
    Result[0] := '0E'; Result[1] := '4E'; Result[2] := '8E'; Result[3] := 'CE';
  end else if Posicao = 12 then begin
    Result[0] := '0F'; Result[1] := '4F'; Result[2] := '8F'; Result[3] := 'CF';
  end else if Posicao = 13 then begin
    Result[0] := '10'; Result[1] := '50'; Result[2] := '90'; Result[3] := 'D0';
  end else if Posicao = 14 then begin
    Result[0] := '11'; Result[1] := '51'; Result[2] := '91'; Result[3] := 'D1';
  end else if Posicao = 15 then begin
    Result[0] := '12'; Result[1] := '52'; Result[2] := '92'; Result[3] := 'D2';
  end else if Posicao = 16 then begin
    Result[0] := '13'; Result[1] := '53'; Result[2] := '93'; Result[3] := 'D3';
  end else if Posicao = 17 then begin
    Result[0] := '14'; Result[1] := '54'; Result[2] := '94'; Result[3] := 'D4';
  end else if Posicao = 18 then begin
    Result[0] := '15'; Result[1] := '55'; Result[2] := '95'; Result[3] := 'D5';
  end else if Posicao = 19 then begin
    Result[0] := '16'; Result[1] := '56'; Result[2] := '96'; Result[3] := 'D6';
  end else if Posicao = 20 then begin
    Result[0] := '17'; Result[1] := '57'; Result[2] := '97'; Result[3] := 'D7';
  end else if Posicao = 21 then begin
    Result[0] := '18'; Result[1] := '58'; Result[2] := '98'; Result[3] := 'D8';
  end else if Posicao = 22 then begin
    Result[0] := '19'; Result[1] := '59'; Result[2] := '99'; Result[3] := 'D9';
  end else if Posicao = 23 then begin
    Result[0] := '1A'; Result[1] := '5A'; Result[2] := '9A'; Result[3] := 'DA';
  end else if Posicao = 24 then begin
    Result[0] := '1B'; Result[1] := '5B'; Result[2] := '9B'; Result[3] := 'DB';
  end else if Posicao = 25 then begin
    Result[0] := '1C'; Result[1] := '5C'; Result[2] := '9C'; Result[3] := 'DC';
  end else if Posicao = 26 then begin
    Result[0] := '1D'; Result[1] := '5D'; Result[2] := '9D'; Result[3] := 'DD';
  end else if Posicao = 27 then begin
    Result[0] := '1E'; Result[1] := '5E'; Result[2] := '9E'; Result[3] := 'DE';
  end else if Posicao = 28 then begin
    Result[0] := '1F'; Result[1] := '5F'; Result[2] := '9F'; Result[3] := 'DF';
  end else if Posicao = 29 then begin
    Result[0] := '20'; Result[1] := '60'; Result[2] := 'A0'; Result[3] := 'E0';
  end else if Posicao = 30 then begin
    Result[0] := '21'; Result[1] := '61'; Result[2] := 'A1'; Result[3] := 'E1';
  end else if Posicao = 31 then begin
    Result[0] := '22'; Result[1] := '62'; Result[2] := 'A2'; Result[3] := 'E2';
  end else if Posicao = 32 then begin
    Result[0] := '23'; Result[1] := '63'; Result[2] := 'A3'; Result[3] := 'E3';
  end else if Posicao = 33 then begin
    Result[0] := '24'; Result[1] := '64'; Result[2] := 'A4'; Result[3] := 'E4';
  end else if Posicao = 34 then begin
    Result[0] := '25'; Result[1] := '65'; Result[2] := 'A5'; Result[3] := 'E5';
  end else if Posicao = 35 then begin
    Result[0] := '26'; Result[1] := '66'; Result[2] := 'A6'; Result[3] := 'E6';
  end else if Posicao = 36 then begin
    Result[0] := '27'; Result[1] := '67'; Result[2] := 'A7'; Result[3] := 'E7';
  end else if Posicao = 37 then begin
    Result[0] := '28'; Result[1] := '68'; Result[2] := 'A8'; Result[3] := 'E8';
  end else if Posicao = 38 then begin
    Result[0] := '29'; Result[1] := '69'; Result[2] := 'A9'; Result[3] := 'E9';
  end else if Posicao = 39 then begin
    Result[0] := '2A'; Result[1] := '6A'; Result[2] := 'AA'; Result[3] := 'EA';
  end else if Posicao = 40 then begin
    Result[0] := '2B'; Result[1] := '6B'; Result[2] := 'AB'; Result[3] := 'EB';
  end else if Posicao = 41 then begin
    Result[0] := '2C'; Result[1] := '6C'; Result[2] := 'AC'; Result[3] := 'EC';
  end else if Posicao = 42 then begin
    Result[0] := '2D'; Result[1] := '6D'; Result[2] := 'AD'; Result[3] := 'ED';
  end else if Posicao = 43 then begin
    Result[0] := '2E'; Result[1] := '6E'; Result[2] := 'AE'; Result[3] := 'EE';
  end else if Posicao = 44 then begin
    Result[0] := '2F'; Result[1] := '6F'; Result[2] := 'AF'; Result[3] := 'EF';
  end else if Posicao = 45 then begin
    Result[0] := '30'; Result[1] := '70'; Result[2] := 'B0'; Result[3] := 'F0';
  end else if Posicao = 46 then begin
    Result[0] := '31'; Result[1] := '71'; Result[2] := 'B1'; Result[3] := 'F1';
  end else if Posicao = 47 then begin
    Result[0] := '32'; Result[1] := '72'; Result[2] := 'B2'; Result[3] := 'F2';
  end else if Posicao = 48 then begin
    Result[0] := '33'; Result[1] := '73'; Result[2] := 'B3'; Result[3] := 'F3';
  end;
end;

end.
