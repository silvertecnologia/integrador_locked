unit U_Produto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmProduto = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    EdtValorPadrao: TEdit;
    EdtDescricao: TEdit;
    Panel1: TPanel;
    BtnCancelar: TBitBtn;
    BtnConfirmar: TBitBtn;
    BtnAtualizar: TBitBtn;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmProduto: TFrmProduto;

implementation

{$R *.dfm}

procedure TFrmProduto.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    if Trim(EdtDescricao.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe a descri��o.');
      EdtDescricao.SetFocus;
    end else if Trim(EdtValorPadrao.Text) = EmptyStr then begin
      CanClose := False;
      ShowMessage('Informe o valor padr�o.');
      EdtValorPadrao.SetFocus;
    end else begin
      try
        StrToFloat(EdtValorPadrao.Text);
      except
        CanClose := False;
        ShowMessage('Valor padr�o inv�lido.');
        EdtValorPadrao.SetFocus;
      end;
    end;
  end;
end;

end.
